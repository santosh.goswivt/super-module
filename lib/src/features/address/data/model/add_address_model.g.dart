// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_address_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddAddressModel _$AddAddressModelFromJson(Map<String, dynamic> json) =>
    AddAddressModel(
      json['ok'] as bool?,
      json['message'] as String,
    );

Map<String, dynamic> _$AddAddressModelToJson(AddAddressModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'message': instance.message,
    };
