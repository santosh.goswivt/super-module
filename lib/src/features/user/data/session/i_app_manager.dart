abstract class IAppManager {
  Future<bool> isAppFirstTime();

  Future<void> saveBool({required String key, required bool value});

  Future<String> getDeviceId();

  Future<void> initiateDeviceIpAddress();

  Future<String?> getDeviceIpAddress();
}
