## [0.0.1] - 1 FEB 2022
- Initial public release

## [0.0.2] - 1 FEB 2022
- Code optimizedN

## [0.0.3] - 1 FEB 2022
- Added GlobalResponseModel
- code refactor

## [0.0.4] - 2 FEB 2022
- Removed android and ios folder dependency & optimized

## [0.0.5] - 2 FEB 2022
- Resolved shared preferences dependency issue on session manager

## [0.0.6] - 2 FEB 2022
- Downgraded bloc to version 7.x.x due to major changes in version 8.x.x

## [0.0.7] - 2 FEB 2022
- Added additional export

## [0.0.8] - 3 FEB 2022
- Refactor of code & additional export

## [0.0.9] - 3 FEB 2022
- Refactor of models

## [0.1.0] - 3 FEB 2022
- Added changed log & export new models

## [0.1.1] - 3 FEB 2022
- Removed rsa public and private keys.

## [0.1.2] - 3 FEB 2022
- Removed rsa public and private keys.

## [0.1.3] - 4 FEB 2022
- Updates in user model profile for point and subscribed countries.

## [0.1.4] - 7 FEB 2022
 - Forgot password model changes
 - some auth stability improvements.

## [0.1.5] - 7 FEB 2022
- Fixed null safety
- Fixed compile time warnings
- Removed map_extension (not required because package is already using sound null safety)
- Firebase push notification code cleanup & better implementation.
- Fixed suggestion warnings.
- Code refactor with dartfmt


## [0.3.5] - 27 FEB 2022
- Fixed Biometrics issue

## [0.3.6] - 3 MAR 2022
- Added verification with email

## [0.3.8] - 3 MAR 2022
- Removed testbloc
- Code refactor
- Build runner DI
- Dart analysis optimized

# [0.3.9] - 11 MAR 2022
- fixed typecast error

# [0.4.14] - 28 MAR 2022
- Gallery permission set true for limited as well.
- Removed app_launcher which is causing issue on application.
- app_launcher now needs to be use from app.

# [0.5.0] - 18 MAY 2022
- Added wishlist
- Added cart
- Code refactor
- Updated packages

# [0.5.3] - 24 MAY 2022
- Added chat